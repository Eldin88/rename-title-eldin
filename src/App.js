import React, {useState, useEffect} from 'react';

const Rename = () => {
 const [name, setName] = useState('');
 const onChange = e => setName(e.target.value);

 useEffect(() => {
    document.title = name;
  }, [name]);

    return  <div class="valeur">
                <h3>Change the title of the page here</h3>
                <input value={name} onChange={onChange} />
            </div>
};

export default Rename;